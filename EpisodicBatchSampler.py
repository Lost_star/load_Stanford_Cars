import torch


class EpisodicBatchSampler(object):
    def __init__(self, n_classes, n_way, n_episodes):
        self.n_classes = n_classes  # 4112
        self.n_way = n_way  # 60
        self.n_episodes = n_episodes  # 100

    def __len__(self):
        return self.n_episodes

    def __iter__(self):
        random_list = torch.randperm(self.n_classes)

        # yield random_list[0:self.n_way]
        for i in range(self.n_episodes):
            if (i + 1) * self.n_way >= self.n_classes:
                # yield torch.randperm(self.n_classes)[:self.n_way]
                yield random_list[self.n_classes - self.n_way:self.n_classes]
            else:
                yield random_list[i * self.n_way:(i + 1) * self.n_way]

        # # 老版本 有些类出现多次 有些类就一次
        # for i in range(self.n_episodes):
        #     yield torch.randperm(self.n_classes)[:self.n_way]
        #     # 从4112个class里选60个
