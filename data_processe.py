import glob
import os
from functools import partial

import numpy as np
import torch
from PIL import Image
from icecream import ic
from torchnet.dataset import ListDataset, TransformDataset  # ListDataset将list类型数据处理成Dataset类
from torchnet.transform import compose
from torchvision import transforms as tfs

def convert_dict(k, v):
    return {k: v}


def load_image_path(key, out_field, d):
    d[out_field] = Image.open(d[key]).convert('RGB')
    return d


def rotate_image(key, rot, d):
    d[key] = d[key].rotate(rot)
    return d


def scale_image(key, height, width, d):
    d[key] = d[key].resize((height, width))  # 原来大小不一定
    return d


def scale_images(key, height, width, d):
    # 直接resize
    d[key] = d[key].resize((height, width))
    return d


def convert_tensors(key, d):
    # d[key] = 1.0 - torch.from_numpy(np.array(d[key], np.float32, copy=False)).transpose(0, 1).contiguous().view(1, d[key].size[0], d[key].size[1])
    # torch.Size([28, 28])-> 转置->连续 ->添加维度变成(1, 28, 28) -> 然后数值交换0和1

    d[key] = torch.from_numpy(np.array(d[key], dtype=np.float32)).transpose(0, 2).contiguous()

    return d


def convert_tensor(key, d):
    # d[key] = 1.0 - torch.from_numpy(np.array(d[key], np.float32, copy=False)).transpose(0, 1).contiguous().view(1, d[key].size[0], d[key].size[1])
    # torch.Size([28, 28])-> 转置->连续 ->添加维度变成(1, 28, 28) -> 然后数值交换0和1

    shape = np.array(d[key]).shape  # 如果是彩色图片就是(28,28,3) 黑白图片就是(28,28)
    # if shape != [28, 28, 3]:
    #     print(d)
    if shape.__len__() != 3:  # 黑白图片
        temp = np.random.randn(3, shape[0], shape[1])
        temp[:] = np.array(d[key], np.float32, copy=False)
        d[key] = torch.from_numpy(temp).unsqueeze(0).contiguous()
        return d

    d[key] = torch.from_numpy(np.array(d[key], np.float32, copy=False)).transpose(0, 2).contiguous()
    # print(d[key].shape)
    return d


def load_class_images(scale_image_size, d):
    # {'class': '/media/idea/c9ae2b5e-60d5-4916-8e02-23f584456660/DataSet/CUB_200_2011/CUB_200_2011/images/037.Acadian_Flycatcher'}

    image_dir = d['class']
    class_images = sorted(
        glob.glob(os.path.join(image_dir, '*.png')) + glob.glob(os.path.join(image_dir, '*.jpg')))  # 目录下所有png&img图片的地址

    if len(class_images) == 0:
        raise Exception("No images found for  {}. ".format(d['class'], image_dir))

    image_ds = TransformDataset(ListDataset(class_images),
                                compose([partial(convert_dict, 'file_name'),
                                         partial(load_image_path, 'file_name', 'data'),
                                         partial(enhance_picture, 'data'),
                                         partial(scale_images, 'data', scale_image_size, scale_image_size),
                                         partial(convert_tensors, 'data')]))

    loader = torch.utils.data.DataLoader(image_ds, batch_size=len(image_ds), shuffle=False)

    result = {'class': d['class']}
    for sample in loader:
        result["data"] = sample['data']
        break  # only need one sample because batch size equal to dataset length

    return result


def extract_episode(n_support, n_query, d):
    # d:{   "class":'Atemayar_Qelisayer/character12/rot180',   "data":(70,3,28,28)}
    n_examples = d['data'].size(0)  # 70

    if n_query == -1:
        n_query = n_examples - n_support  # 70个样本，去掉support剩下的作为query

    # ic(n_support + n_query)
    example_inds = torch.randperm(n_examples)[:(n_support + n_query)]  # 打乱所有样本，取n_support+n_query个
    support_inds = example_inds[:n_support]  # support
    query_inds = example_inds[n_support:]  # query

    xs = d['data'][support_inds]
    xq = d['data'][query_inds]

    return {
        'class': d['class'],
        'xs': xs,
        'xq': xq
    }


def enhance_picture(key, d):
    """
    图片增强
    :param d:字典，包括'file_name'和'data'
    https://pytorch.org/docs/0.3.0/torchvision/transforms.html
    """
    # print(d)
    img = d[key]
    # save(img)
    # 随机水平翻转
    img = tfs.RandomHorizontalFlip()(img)
    # save(img)
    # 亮度变化
    # img = tfs.ColorJitter(brightness=1)(img)
    # save(img)
    # 对比度变化
    # img = tfs.ColorJitter(contrast=2)(img)
    # save(img)
    # 饱和度变化
    # img = tfs.ColorJitter(saturation=1)(img)
    # save(img)
    # 颜色变化
    # img = tfs.ColorJitter(hue=1)(img)

    # 随机角度旋转
    # img = tfs.RandomRotation(30)(img)
    # save(img)

    d[key] = img

    return d
