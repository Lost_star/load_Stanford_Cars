import scipy.io as scio
import os
import os.path as osp
import numpy as np
from torch.autograd import Variable

from EpisodicBatchSampler import *
from data_processe import *

# 3
dataset_path = "

train_img_path = osp.join(dataset_path, "cars_train")
test_img_path = osp.join(dataset_path, "cars_test")

label_path = osp.join(dataset_path, "car_devkit", "devkit")
train_label_path = osp.join(label_path, "cars_train_annos.mat")
test_label_path = osp.join(label_path, "cars_test_annos.mat")

img = 0


def devide_dataset_by_label(path):
    global img
    data = scio.loadmat(path)
    data = data["annotations"]
    data = data.squeeze()
    for number, _label in enumerate(data):
        label = _label[0][0][0]
        if not osp.exists(osp.join(dataset_path, "classes", str(int(label)))):
            os.makedirs(osp.join(dataset_path, "classes", str(int(label))))
        os.system(
            f"cp {osp.abspath(osp.join(train_img_path, str(int(number + 1)).zfill(5) + '.jpg'))} {osp.abspath(osp.join(dataset_path, 'classes', str(int(label))))}")
        img += 1


def get_Stanford_Cars_dataloader(mode="train", way=5, shot=2, query=10):
    if not osp.exists(osp.abspath(osp.join(dataset_path, "classes"))):
        devide_dataset_by_label(train_label_path)
        devide_dataset_by_label(test_label_path)
        print(f"devide {img} imgs")
    classes_path = osp.abspath(osp.join(dataset_path, "classes"))

    class_list = []
    for class_name in os.listdir(classes_path):
        if class_name.__contains__("DS_Store"):
            continue
        class_list.append(os.path.join(classes_path, class_name))
    class_names = []
    for i in class_list:
        if os.listdir(i).__len__() >= query + shot:
            class_names.append(i)

    train_class_lists = class_names[:int(class_names.__len__() * 0.6)]
    val_class_lists = class_names[int(class_names.__len__() * 0.6):int(class_names.__len__() * 0.8)]
    test_class_lists = class_names[int(class_names.__len__() * 0.8):]

    transforms = [partial(convert_dict, "class"),
                  partial(load_class_images, 64),
                  partial(extract_episode, shot, query)]

    transforms = compose(transforms)

    lists = []
    if mode == "train":
        lists = train_class_lists
    elif mode == "val":
        lists = val_class_lists
    elif mode == "test":
        lists = test_class_lists

    episode = int(len(lists) / way) + 1  # 加1防止有一类没学习

    ds = TransformDataset(ListDataset(lists),  # 先将list类型数的class_names处理成Dataset类的ListDataset
                          transforms)  # 然后对它使用transforms,对数据进行进一步处理

    sampler = EpisodicBatchSampler(len(ds), way, episode)  # 每个episode有10way，每个batch有30个episdoe

    dataloader = torch.utils.data.DataLoader(ds, batch_sampler=sampler, num_workers=0)

    return dataloader


if __name__ == '__main__':
    dataloader = get_Stanford_Cars_dataloader(way=2, shot=3, query=1)
    for i, sample in enumerate(dataloader):
        """
        sample is a dict
        sample["class"] : class_path
        sample["xs"] : (way,shot,3,64,64)
        sample["xq"] : (way,query,3,64,64)
        """
        xs = Variable(sample["xs"])
        xq = Variable(sample["xq"])

        xs_label = torch.arange(0, xs.shape[0]).view(xs.shape[0], 1).expand(xs.shape[0], xs.shape[1]).long()  # torch.Size([5, 2])
        xq_label = torch.arange(0, xq.shape[0]).view(xq.shape[0], 1).expand(xq.shape[0], xq.shape[1]).long()  # torch.Size([5, 10])

        break

